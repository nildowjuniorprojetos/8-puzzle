import com.google.common.base.Objects;

import java.awt.*;

public class Coordinate {
    private Point coordinate;

    public Coordinate(int x, int y) {
        this.coordinate = new Point(x, y);
    }

    public int getX() {
        return (int) this.coordinate.getX();
    }

    public int getY() {
        return (int) this.coordinate.getY();
    }

    public int moveX(MovePossibilities move) {
        int newX = this.getX();
        switch (move) {
            case LEFT:
                newX = newX - 1;
                break;
            case RIGHT:
                newX = newX + 1;
                break;
        }
        return newX;
    }

    public int moveY(MovePossibilities move) {
        int newY = this.getY();
        switch (move) {
            case TOP:
                newY = newY - 1;
                break;
            case DOWN:
                newY = newY + 1;
                break;
        }
        return newY;
    }

    @Override
    public String toString() {
        return "{" + this.getX() + ", " + this.getY() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return Objects.equal(coordinate, that.coordinate);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(coordinate);
    }
}
