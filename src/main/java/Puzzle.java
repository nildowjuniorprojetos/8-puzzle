import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Puzzle {

    private final StateComparator COMPARATOR = new StateComparator();
    private Map<String, State> border = new HashMap<>();
    private Map<String, State> visited = new HashMap<>();
    private PriorityQueue<State> costs = new PriorityQueue<>(1, COMPARATOR);
    private State GOAL;
    private int countStatesChecked;
    private int maxBorderNumber;

    public Puzzle(State initial, State goal) {
        this.GOAL = goal;
        this.countStatesChecked = 0;
        this.maxBorderNumber = 0;
        this.border.put(initial.getKey(), initial);
        this.costs.add(initial);
    }

    public Map<Integer, Coordinate> findSolution() {
        State finalState = null;

        while (finalState == null && !this.border.isEmpty()) {
            finalState = this.go();
        }

        if (finalState == null) {
            return null;
        }

        finalState.getGenealogy().forEach(State::printBoard);

        return finalState.getPath();
    }

    private State go() {
        updateMaxBorderSize();
        State state = this.border.remove(costs.poll().getKey());

        if (state != null) {

            updateCountStatesChecked();
            if (state.equals(this.GOAL)) {
                return state;
            } else {
                this.visited.put(state.getKey(), state);

                state.expand().stream()
                        .forEach(expanded -> {
                            State inBorder = this.border.get(expanded.getKey());

                            if ((inBorder == null || inBorder.getTotalCost() > expanded.getTotalCost())
                                    && !visited.containsKey(expanded.getKey())) {

                                this.border.put(expanded.getKey(), expanded);
                                this.costs.add(expanded);
                            }
                        });
            }
        }
        return null;
    }

    private void updateCountStatesChecked() {
        this.countStatesChecked++;
    }

    private void updateMaxBorderSize() {
        int borderSize = this.border.size();
        if (this.maxBorderNumber < borderSize) {
            this.maxBorderNumber = borderSize;
        }
    }

    public int getCountStatesChecked() {
        return this.countStatesChecked;
    }

    public int getMaxBorderNumber() {
        return this.maxBorderNumber;
    }

}


