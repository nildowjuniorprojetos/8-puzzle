import java.util.Date;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {

	private static final String TOTAL_DE_ESTADOS_EXPLORADOS = "Total de estados explorados até encontrar a solução --> %d";
	private static final String MAXIMO_ESTADOS_NA_FRONTEIRA = "O maior número de estados na fronteira foi --> %d";
	private static final String NAO_FOI_ENCONTRADA_SOLUCAO = "Não foi encontrada solução!";
	private static final String TEMPO_TOTAL = "TEMPO TOTAL: %d milissegundos";
	private static final String SEM_SOLUCAO = "Estado inicial não possui solução";
	private static final String ENTRADA_ESTADO_INICIAL = "\n\nInforme a sequência dos números. Sem nenhum tipo de separação. Utilize 0 para o espaço vazio\n";
	private static final String QUANTIDADE_POSICOES_INVALIDA = "Quantidade de posições informada é inválida!";
	private static final String TIPO_DADOS_INVALIDOS = "Você deve digitar apenas números!";
	private static final String DADOS_INVALIDOS = "Entrada de dados inválida!";
	private static Scanner scan = new Scanner(System.in);
	private static final int SIZE = 3;
	private static final int TOTAL_BOARD_SIZE = SIZE * SIZE;

	public static void main(String[] args) {
		while (true) {
			try{
				int[] numeros = getNumberInputData();

				Date startTime = new Date();

				int[][] goalBoard = new int[SIZE][SIZE];

				goalBoard[0][0] = 1;
				goalBoard[1][0] = 2;
				goalBoard[2][0] = 3;
				goalBoard[0][1] = 4;
				goalBoard[1][1] = 5;
				goalBoard[2][1] = 6;
				goalBoard[0][2] = 7;
				goalBoard[1][2] = 8;
				goalBoard[2][2] = 0;

				State GOAL = new State(goalBoard);

				int[][] initialBoard = new int[SIZE][SIZE];

				int count = 0;
				for(int y = 0; y < SIZE; y++){
					for(int x = 0; x < SIZE; x++){
						initialBoard[x][y] = numeros[count++];
					}
				}

				State stateInitial = new State(initialBoard, null, GOAL, null);

				if (stateInitial.hasSolution()) {

					Puzzle puzzle = new Puzzle(stateInitial, GOAL);

					Map<Integer, Coordinate> path = puzzle.findSolution();
					if (path != null) {
						Set<Integer> integers = path.keySet();

						integers.stream().forEach(i -> {
							System.out.println(String.format("%s - %s", i.toString(), path.get(i)));
						});

						System.out.println(String.format(TOTAL_DE_ESTADOS_EXPLORADOS, puzzle.getCountStatesChecked()));
						System.out.println(String.format(MAXIMO_ESTADOS_NA_FRONTEIRA, puzzle.getMaxBorderNumber()));
					} else {
						System.out.println(NAO_FOI_ENCONTRADA_SOLUCAO);
					}

					Long tempo = new Date().getTime() - startTime.getTime();
					System.out.println(String.format(TEMPO_TOTAL, tempo));
				} else {
					System.out.println(SEM_SOLUCAO);
				}
			}catch(InvalidDataInputException e){
				System.out.println(e.getMessage());
			}
		}
	}

	private static int[] getNumberInputData() throws InvalidDataInputException {
		String dados = getStringInputData();
		if (dados.length() == TOTAL_BOARD_SIZE) {
			try {
				int[] numeros = dados.chars()
						.mapToObj(c -> (char) c)
						.map(Object::toString)
						.mapToInt(Integer::valueOf).toArray();
				if (isValidInputData(numeros)) {
					return numeros;
				} else {
					throw new InvalidDataInputException(DADOS_INVALIDOS);
				}
			} catch (NumberFormatException e) {
				throw new InvalidDataInputException(TIPO_DADOS_INVALIDOS);
			}
		} else {
			throw new InvalidDataInputException(QUANTIDADE_POSICOES_INVALIDA);
		}
	}

	private static String getStringInputData() {
		System.out.print(ENTRADA_ESTADO_INICIAL);
		String str = "";
		str = scan.nextLine();
		return str;
	}

	private static boolean isValidInputData(int[] dados) {
		boolean isValid = true;
		for (int i = 0; i < TOTAL_BOARD_SIZE && isValid; i++) {
			isValid = false;
			for (int t = 0; t < TOTAL_BOARD_SIZE && !isValid; t++) {
				if (i == dados[t]) {
					isValid = true;
				}
			}
		}
		return isValid;
	}
}

