import java.util.Comparator;

public class StateComparator implements Comparator<State> {
    public int compare(State o1, State o2) {
        int retorno = o1.getTotalCost() - o2.getTotalCost();
//        if (retorno == 0) {
//            retorno = o1.getHeuristicCost() - o2.getHeuristicCost();
//        }
        return retorno;
    }
}
