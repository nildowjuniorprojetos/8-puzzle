import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.util.*;
import java.util.stream.Stream;

public class State {

    private static final int HEURISTIC_FACTOR = 1;
    private final int SPACE = 0;

    private int[][] board;
    private State parent;
    private Integer heuristicCost;
    private Integer pathCost;
    private int BOARD_SIZE;
    private State GOAL;
    private String stateStr;
    private Map<Integer, Coordinate> coordinates;

    public State(int[][] board, State parent, State goal, Integer newTotalHeuristic) {
        this.board = board;
        this.parent = parent;
        this.GOAL = goal;
        this.BOARD_SIZE = this.board.length;
        this.heuristicCost = MoreObjects.firstNonNull(newTotalHeuristic, this.calculateHeuristic());
        this.stateStr = this.toString();
        this.resolvePathCost();
        this.mapCoordinates();
    }

    public State(int[][] board) {
        this.board = board;
        this.BOARD_SIZE = this.board.length;
        this.stateStr = this.toString();
        this.mapCoordinates();
    }

    private Integer calculateHeuristic() {
        int sum = 0;

        for (int y = 0; y < this.BOARD_SIZE; y++) {
            for (int x = 0; x < this.BOARD_SIZE; x++) {
                if(this.board[x][y] != 0){
                    sum += heuristicForValue(this.board[x][y], new Coordinate(x, y));
                }
            }
        }

        return sum;
    }

    private int heuristicForValue(Integer value, Coordinate coordinate) {
        Coordinate goalCoordinate = this.GOAL.getCoordinate(value);
        int deltaX = Math.abs(goalCoordinate.getX() - coordinate.getX());
        int deltaY = Math.abs(goalCoordinate.getY() - coordinate.getY());
        return deltaX * HEURISTIC_FACTOR + deltaY * HEURISTIC_FACTOR;
    }

    private void mapCoordinates() {
        this.coordinates = new HashMap<>();
        int[][] board = this.getBoard();
        for (int y = 0; y < this.getBoardSize(); y++) {
            for (int x = 0; x < this.getBoardSize(); x++) {
                this.coordinates.put(board[x][y], new Coordinate(x, y));
            }
        }
    }

    public Map<Integer, Coordinate> getPath() {
        Map<Integer, Coordinate> map = new HashMap<>();
        map.put(this.getPathCost(), this.getCoordinate(this.SPACE));
        if (this.parent != null) {
            Map<Integer, Coordinate> parentPath = this.parent.getPath();
            map.putAll(parentPath);
        }
        return map;
    }

    public State moveSpaceTo(MovePossibilities move) {
        int[][] newBoard = this.copy(this.board);

        final Coordinate spaceCoordinate = getCoordinate(this.SPACE);
        int x = spaceCoordinate.moveX(move);
        int y = spaceCoordinate.moveY(move);

        int oldValue = newBoard[x][y];
        newBoard[spaceCoordinate.getX()][spaceCoordinate.getY()] = oldValue;
        newBoard[x][y] = this.SPACE;

        Integer oldSpaceHeuristic = this.heuristicForValue(this.SPACE, spaceCoordinate);
        Integer newSpaceHeuristic = this.heuristicForValue(this.SPACE, new Coordinate(x, y));

        Integer oldValueHeuristic = this.heuristicForValue(oldValue, new Coordinate(x, y));
        Integer newValueHeuristic = this.heuristicForValue(oldValue, spaceCoordinate);

        Integer newTotalHeuristic = this.getHeuristicCost() -
                oldSpaceHeuristic - oldValueHeuristic +
                newSpaceHeuristic + newValueHeuristic;

        return new State(newBoard, this, this.GOAL, newTotalHeuristic);
    }

    private int[][] copy(int[][] boardToCopy) {
        int[][] newBoard = new int[this.BOARD_SIZE][this.BOARD_SIZE];
        for (int y = 0; y < this.BOARD_SIZE; y++) {
            for (int x = 0; x < this.BOARD_SIZE; x++) {
                newBoard[x][y] = boardToCopy[x][y];
            }
        }
        return newBoard;
    }

    public List<State> expand() {
        List<State> expandeds = new ArrayList<>();
        List<MovePossibilities> movePossibilities = this.getMovePossibilities();
        movePossibilities.stream().forEach(possibility -> {
            State state = this.moveSpaceTo(possibility);
            expandeds.add(state);
        });
        return expandeds;
    }

    private List<MovePossibilities> getMovePossibilities() {
        ArrayList<MovePossibilities> movePossibilities = new ArrayList<>();
        Coordinate coordinate = this.getCoordinate(this.SPACE);
        if (coordinate.getX() != 0) {
            movePossibilities.add(MovePossibilities.LEFT);
        }
        if (coordinate.getX() != this.BOARD_SIZE - 1) {
            movePossibilities.add(MovePossibilities.RIGHT);
        }
        if (coordinate.getY() != 0) {
            movePossibilities.add(MovePossibilities.TOP);
        }
        if (coordinate.getY() != this.BOARD_SIZE - 1) {
            movePossibilities.add(MovePossibilities.DOWN);
        }

        return movePossibilities;
    }

    public void printBoard() {
        String output = stateStr;
        System.out.println(output);
    }

    public void printPossibility() {
        String output = stateStr;
        output = output.replaceAll("^\\|", "\t\t|");
        System.out.println(output.replaceAll("\\n\\|", "\n\t\t|"));
    }

    @Override
    public String toString() {
        String output = "";
        for (int y = 0; y < this.BOARD_SIZE; y++) {
            for (int x = 0; x < this.BOARD_SIZE; x++) {
                Integer number = this.board[x][y];
                output += "|" + (number != 0 ? number : " ");
            }
            output += "|\n";
        }
        return output;
    }

    private void resolvePathCost() {
        if (this.parent != null) {
            this.pathCost = this.parent.getPathCost() + 1;
        } else {
            this.pathCost = 0;
        }
    }

    /**
     * Original code: http://www.geeksforgeeks.org/check-instance-8-puzzle-solvable/
     * @return true if initial state has solution
     */
    public boolean hasSolution() {
        int[] concat = new int[this.BOARD_SIZE * this.BOARD_SIZE];
        int count = 0;
        for (int y = 0; y < this.BOARD_SIZE; y++) {
            for (int x = 0; x < this.BOARD_SIZE; x++) {
                concat[count++] = this.board[x][y];
            }
        }
        int inv_count = 0;
        for (int i = 0; i < concat.length - 1; i++)
            for (int j = i + 1; j < concat.length; j++)
                if (concat[j] != 0 && concat[i] != 0 && concat[i] > concat[j])
                    inv_count++;
        return inv_count % 2 == 0;
    }

    public String getKey() {
        return this.stateStr;
    }

    public Coordinate getCoordinate(Integer value) {
        return this.coordinates.get(value);
    }

    public Integer getPathCost() {
        return pathCost;
    }

    public Integer getTotalCost() {
        return this.heuristicCost + this.pathCost;
    }

    public int[][] getBoard() {
        return board;
    }

    public State getParent() {
        return parent;
    }

    public Integer getHeuristicCost() {
        return heuristicCost;
    }

    public int getBoardSize() {
        return this.BOARD_SIZE;
    }

    public Stream<State> getGenealogy() {
        Stream<State> genealogy = Stream.of(this);
        return parent != null ? Stream.concat(parent.getGenealogy(), genealogy) : genealogy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return Arrays.deepEquals(board, state.board);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.board);
    }
}
